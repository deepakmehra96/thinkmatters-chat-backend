const mongoose = require('mongoose')

const conversationSchema = new mongoose.Schema({
    to: String,
    from: String,
    conversationId: mongoose.Schema.ObjectId,
    value: String,
    image: String,
    file: String,
},{ timestamps: { createdAt: 'createdAt' } })

module.exports = mongoose.model('message', conversationSchema);