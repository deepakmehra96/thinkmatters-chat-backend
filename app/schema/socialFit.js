var mongoose = require("mongoose");

var socialFitSchema = mongoose.Schema({
  userId: String,
  type: String,
  refreshToken: String,
  sinceTime: String,
  // afterDate: String
});

let socialFit = mongoose.model("socialFit", socialFitSchema);
module.exports = socialFit;
