const mongoose = require('mongoose')

const conversationSchema = new mongoose.Schema({
    participants: Array,
    toMemberName: String,
    fromMemberName: String,
    participantsList: Array,
},{ timestamps: { createdAt: 'createdAt' } })

module.exports = mongoose.model('conversation', conversationSchema);