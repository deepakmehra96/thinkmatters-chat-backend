const Conversation = require('../schema/conversation')
const AsyncOperations = require('./AsyncOperations')

module.exports = new AsyncOperations(Conversation)