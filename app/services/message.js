const Message = require('../schema/message')
const AsyncOperations = require('./AsyncOperations')

module.exports = new AsyncOperations(Message)