const socialFit = require('../schema/socialFit')
const AsyncOperations = require('./AsyncOperations')

module.exports = new AsyncOperations(socialFit)