class AsyncQuery {

    constructor (model) {
        this.model = model
        this.findQuery = this.findQuery.bind(this)
        this.remove = this.remove.bind(this)

    }

    findQuery (method, query) {
        return new Promise (
            (
                resolve, reject
            ) => {
                this.model[method](
                    query, (err, document) => {
    
                        if (err) {
                            reject(err)
                        } else {
                            resolve(document)
                        }
                    }
                )
            }
        )
    }

    remove(method, query) {
        return new Promise (
            (
                resolve, reject
            ) => {
                this.model[method](
                    query, (err, list) => {

                        if(err) {
                            reject(err)
                        }
                        else {
                            resolve(list)
                        }

                    }
                )
            }
        )
    }

}

module.exports = AsyncQuery