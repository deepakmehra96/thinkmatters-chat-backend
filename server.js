var express = require("express");
let axios = require('axios')
let mongoose = require('mongoose');
var moment = require('moment');
var bodyParser = require("body-parser");
var app = express();
var unirest = require("unirest");
var server = require("http").Server(app);
const https = require("https");
var io = require("socket.io")(server, { pingTimeout: 30000 });
var path = require("path");
var port = 3041;
var router = express.Router();
var config = require("./config");
let now = require('nano-time');
let socialFit = require('./app/schema/socialFit');
var FormData = require('form-data')
var qs = require('qs');
var request = require('request')
var cron = require('node-cron');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

mongoose.connect("mongodb://localhost:27017/thinkMatterChat", {
  useMongoClient: true
}).then(() => {
    console.log("connected to db");
  })
  .catch(err => console.error(err));

// mongoose
//   .connect("mongodb://127.0.0.1:27017/thinkMatterChat", {
//     useMongoClient: true
//   })
  // .then(() => {
  //   console.log("connected to db");
  // })
  // .catch(err => console.error(err));

mongoose.Promise = global.Promise;

const ConversationServices = require("./app/services/conversation");
const MessageServices = require("./app/services/message");
const SocialFitServices = require("./app/services/socialFit");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set("superSecret", config.secret);

app.use(express.static(path.join(__dirname, "public")));

app.get("/", function (req, res, next) {
  res.sendFile(__dirname + "/index.html");
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

let users = [];

function findUser(userId) {
  return users.find(user => user.userId == userId);
}

app.post("/new", async function (req, res, next) {
  const existingConversation = await ConversationServices.findOne({
    participants: {
      $all: req.body.participants
    }
  })
  if (!existingConversation) {
    const conversation = await ConversationServices.save(req.body)
    res.json(conversation)
  } else {
    res.json(existingConversation)
  }
})


io.on("connection", function (socket) {
  console.log(socket.id, "id")

  socket.on("new_connection", function (data) {
    console.log(data, "datattata")
    users.push({
      userId: data.userId,
      socketId: socket.id
    })

    console.log(socket.id, "id");
    socket.emit("id_assigned", { id: socket.id })
  })
  socket.on("disconnect", function (data) {
    users = users.filter(user => user.socketId !== socket.id)
  })
  socket.on("sendMessage", async function (data) {
    const user = findUser(data.to)
    if (user) {
      socket.broadcast.to(user.socketId).emit("receivedMessage", data)
    }
    const message = await MessageServices.save(data)
  })
})

// app.get('/conversations/:id', async function (req, res) {
// 	const existingConversation = await ConversationServices.findOne({
// 		_id: req.params.id
// 	})
// 	const messages = await MessageServices.find({
// 		conversationId: req.params.id
// 	}) || []

// 	if (existingConversation) {
//         res.json({ conversation: existingConversation || {}, messages: messages})

// 	}else{
// 		res.json({ message: 'no conversation found' })
// 	}
// })

app.get("/conversations/:id/user/:userId", async function (req, res, next) {
  const existingConversation = await ConversationServices.findOne({
    _id: req.params.id
  })
  const messages =
    (await MessageServices.find({
      conversationId: req.params.id
    })) || []

  if (existingConversation) {
    res.json({ conversation: existingConversation || {}, messages: messages });
  } else {
    res.json({ message: "no conversation found" });
  }
})




// app.post('/userData', (req, res) => {
//   let id = 'akash'
//   let type = 'google fit'
//   let refreshToken = "1//0gvDPkixKIucMCgYIARAAGBASNwF-L9IrXWYYoIErHgB5488Znpd0JwihhLgYR3Aj8DqypzGHodzK9oToS1j7NY2ffDTG7NE1x1g"

//   socialFit.find({ userId: id}, (err, data) =>{
//     if(err){
//       console.log(err)
//     }else{
//       if(!data.length){
//         let socialFitData = new socialFit({
//           userId: id,
//           type: type,
//           // accessToken: "test",
//           refreshToken: refreshToken
//         });
//         socialFitData.save((err, data) => {
//           if (err) {
//             console.log("err --==>", err);
//           }
//           console.log("Book successfully saved.", data);
//         });
//       }else{
//         updateAccessToken(data[0]._doc.refreshToken)
//       }
//     }
//     res.send("done")
//   })
// });


let getInitialTime = ()  => {
  var date = new Date();
  date.setHours(date.getHours() - date.getHours());
  date.setMinutes(date.getMinutes() - date.getMinutes());
  date.setSeconds(date.getSeconds() - date.getSeconds());
  let initialDate = date.getTime()
  return initialDate
  console.log(" initial date -=>", date.toString())
}

let getFitBitTime = ()  => {
  let date = new Date()
  let year = date.getFullYear().toString();
  let getmonth = () =>{
    let month = (date.getMonth() + 1).toString();
    if(month < 10){
      return ('0'.concat(month))
    }else{
      return month
    }
  }
  let month = getmonth()
  let dates = date.getDate().toString();
  let modifiedDate = year.concat('-').concat(month).concat('-').concat(dates).concat('T00:00:00')
  return modifiedDate
}

let getFitBitSinceTime = ()  => {
  let date = new Date()
  let year = date.getFullYear().toString();
  let getmonth = () =>{
    let month = (date.getMonth() + 1).toString();
    if(month < 10){
      return ('0'.concat(month))
    }else{
      return month
    }
  }
  let month = getmonth()
  let dates = date.getDate().toString();
  let hours = date.getHours().toString();
  let minutes = date.getMinutes().toString();
  let seconds = date.getSeconds().toString();
  let modifiedDate = year.concat('-').concat(month).concat('-').concat(dates).concat('T').concat(hours).concat(':').concat(minutes)
  .concat(":").concat(seconds)
  return modifiedDate
}




app.post('/socailFit',async function (req, res) {
  console.log(req.body.userId,"req.body.userId req.body.userId")
    const existingSocialFitData = await SocialFitServices.findOne({
      participants: {
        userId: req.body.userId
      }
    })
    if (!existingSocialFitData) {
    
      if (req.body.third_party_type == 'google') {
        const setSocialFitData = await SocialFitServices.save({
          userId : req.body.userId,
          type: req.body.third_party_type,
          refreshToken:req.body.third_party_token ,
          sinceTime : new Date().getTime(),
        })
        updateGoogleAccessToken(req.body.third_party_token,  req.body.userId, null, true)
      }else{
        const setSocialFitData = await SocialFitServices.save({
          userId : req.body.userId,
          type: req.body.third_party_type,
          refreshToken:req.body.third_party_token ,
          sinceTime : getFitBitSinceTime()
        })
        updateFitBitAccessToken(req.body.third_party_token, req.body.userId,null, true)
      }
    }
  res.send("done")
})


app.post('/getSocialFitData', async (req, res) =>{
  let userId = req.body.userId
console.log(userId,"userId userId userId")

socialFit.find({userId: userId}, (err, data) =>{
  if(err){
    console.log("err ----====>", err)
  }else{
    console.log("data ----====>", data)
    if (data.length > 0) {
      data = data[0]
      if (data.type == 'google') {
            updateGoogleAccessToken(data && data.refreshToken, data && data.userId, data && data.sinceTime, false )
        }else{
            updateFitBitAccessToken(data && data.refreshToken, data && data.userId, data && data.sinceTime, false)
          }
    }
  }
})

// const existingUsers = await ConversationServices.findOne({
//       userId:userId
//     })
//       if(existingUsers){
//         console.log(data,"data data data")
//         if (data.type == 'google') {
//           updateGoogleAccessToken(data && data.refreshToken, data && data.userId, data && data.sinceTime, false )
//         }else{
//             updateFitBitAccessToken(data && data.refreshToken, data && data.userId, data && data.sinceTime, false)
//           }
//         res.send(data)
//       }
})

cron.schedule('0 0 */24 * * *', () => {
  console.log('running a task every minute')
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("thinkMatterChat");
    dbo.collection("socialfits").findOne({}, function(err, result) {
      if (err) throw err;
      console.log(result);
      if (result) {
        if (result.type == 'google') {
            updateGoogleAccessToken(result && result.refreshToken, result && result.userId, result && result.sinceTime, false )
        }else{
            updateFitBitAccessToken(result && result.refreshToken, result && result.userId, result && result.sinceTime, false)
          }
      }
      db.close()
    })
  })
})


let updateFitBitAccessToken = async (data, userId, sinceTime, isFirst) => {

  data = JSON.parse(data)
  let dataFitbit = {
    grant_type: 'refresh_token',
    refresh_token: data.refreshToken
  }
  var options = {
    'method': 'POST',
    'url': 'https://api.fitbit.com/oauth2/token',
    'headers': {
      'Authorization': 'Basic MjJCRDJXOjJkMzNkYzJjM2RhYTAxNzMzNTcyZTY2YTFjMzc4OTY5',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    form: dataFitbit
  }
  request(options, async function (error, response) { 
    if (error) throw new Error(error);
    console.log(response.body,"111111111111")

    const setSocialFitData = await SocialFitServices.update({'userId': userId},{
      $refreshToken: response.body
    })

    getFitBitCalories(response.body , userId, sinceTime, isFirst)
  })
}


let updateGoogleAccessToken = async (data, userId, sinceTime, isFirst ) => {
  data = JSON.parse(data)
  let client_id = '700217228139-jmft4sfs32b4fibt5gmget6ta52vj1n1.apps.googleusercontent.com';
  let client_secret = '6P74MCD67Z_Ofx3cTCFssGzq';

  console.log({client_id: client_id,
    client_secret: client_secret,
    refresh_token: data.refreshToken,
    grant_type: 'refresh_token'})

    axios.post('https://www.googleapis.com/oauth2/v4/token', {
      client_id: client_id,
      client_secret: client_secret,
      refresh_token: data.refreshToken,
      grant_type: 'refresh_token'
    })
    .then(async res => {

      console.log(res.data,"res.data res.data res.data")

      const setSocialFitData = await SocialFitServices.update({'userId': userId},{
        $refreshToken: res.data
      })
      getGoogleCalory(res.data.access_token,userId, sinceTime, isFirst )
    })
    .catch(error => {
     console.error("err --====>", error)
    })
}


let getGoogleCalory = async (data, userId, sinceTime, isFirst ) => {
  console.log("data --====>", data)
  console.log(" sinceTime --====>",  sinceTime)
  console.log(" isFirst --====>",  isFirst)
  //let isFirst = True
  let initialTime
  if(isFirst){
    initialTime =  getInitialTime()
  }else{
    initialTime = sinceTime
  }
  
  var req = unirest("POST", "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate");
  req.headers({
    "Authorization": "Bearer " + data,
    "Content-Type": "application/json"
  })
  req.type("json");
  req.send({
    "aggregateBy": [
      {
        "dataSourceId": "derived:com.google.calories.expended:com.google.android.gms:platform_calories_expended"
      }
    ],
    "bucketByActivityType": {},
    "startTimeMillis": initialTime,
    "endTimeMillis": new Date().getTime()
  });
  req.end(async function  (res) {
    console.log("res =>", res.body)
   if (res.error || res.body.bucket.length && !res.body.bucket[0].dataset.length && !res.body.bucket[0].dataset[0].point.length){
     console.log("There is no data or something went wrong")
   } else {

     let array = []
     for(let i = 0; i < res.body.bucket.length; i++){
       let obj = {
         activityId: res.body.bucket[i].activity,
         calories: res.body.bucket[i].dataset[0].point[0].value[0].fpVal
       }
       array.push(obj)
     }


     if (array.length > 0 ) {

      const setSocialFitData = await SocialFitServices.update({'userId': userId},{
        sinceTime : new Date().getTime(),
      })

      let dataToSend = {
        json_data: array,
        activity_from: 'googlefit',
        user_id:userId
      }
      console.log(dataToSend," dataToSend dataToSend dataToSend dataToSend")
      axios.post('https://thinkmatters.softuvo.xyz/api/adduserexercisefrom', JSON.stringify(dataToSend)).then(response => {
        console.log(response.statusText,"response nihal api ")
      })
    }
   }
  });
}

let getFitBitCalories = async (data,userId, sinceTime, isFirst ) => {
  data = JSON.parse(data)
   let afterDate
   if(isFirst){
    afterDate = getFitBitTime()
    console.log("after Date -=>", afterDate)
   }else{
    afterDate = sinceTime
   }

   const setSocialFitData = await SocialFitServices.update({'userId': userId},{
    $sinceTime : getFitBitSinceTime()
  })

  axios.get(`https://api.fitbit.com/1/user/${data.user_id}/activities/list.json?afterDate=${afterDate}&sort=desc&offset=0&limit=100`, {
    headers:{ 'Authorization': `Bearer ${data.access_token} `}
  }).then(res => {
    console.log("fit bit res -=>", res.statusText)

    console.log(res.data.activities,"res.data.activities ")
      if (res.data.activities.length > 0 ) {

        let array = []
        for(let i = 0; i < res.data.activities.length; i++){
          let obj = {
            activityId: res.data.activities[i].activityTypeId,
            calories: res.data.activities[i].calories
          }
          array.push(obj)
        }
        
        let dataToSend = {
          json_data: array,
          activity_from: 'fitbit',
          user_id:userId
        }

        console.log(dataToSend,"res.data to nihal nihal nihal ")
        axios.post('https://thinkmatters.softuvo.xyz/api/adduserexercisefrom', JSON.stringify(dataToSend)).then(response => {
          console.log(response,"response nihal api ")
        })
      }
  })
  .catch(error => {
  console.error("err --====>", error)
  })
}

app.get('/checkServer', async function (req, res) {
	res.json({ message: 'Server is Running' })
})

// app.use("/api", router);
// app.listen(port, () => console.log("listening on ", port));  ///// server.listen
// app.listen(8000, () => {
//   console.log('Example app listening on port 8000!')
// });

// app.use('/api', router);
server.listen(port, () => {
	  console.log('Example app listening on port' + port)
	});
